import org.w3c.dom.*;
import org.xml.sax.SAXException;
import videoclub.Videos;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Composición de la ventana
 */
public class Vista {
    private JFrame frame;
    private JPanel panel1;
    private JLabel lblNombrePeli;
    private JTextField NombreTxt;
    private JLabel lblCodigoPeli;
    private JTextField CodigoTxt;
    private JButton altaPeliBtn;
    private JButton mostrarPeliculaBtn;
    private JComboBox comboBoxGenero;

    private JLabel lblPeli;

    private LinkedList<Videos> lista;
    private DefaultComboBoxModel<Videos> dcbm;

    /**
     * Creación de la ventana
     */
    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        crearMenu();

        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        comboBoxGenero.setModel(dcbm);

        altaPeliBtn.addActionListener(new ActionListener() {
            @Override
            /**
             * Da de alta la pelicula y lista las peliculas de la lista
             */
            public void actionPerformed(ActionEvent e) {
                altaPeli(NombreTxt.getText(), CodigoTxt.getText());
                refrescarComboBox();
            }


        });
        mostrarPeliculaBtn.addActionListener(new ActionListener() {
            @Override
            /**
             * Metodo para mostrar las peliculas creadas previamente
             */
            public void actionPerformed(ActionEvent e) {
                //Muestro la película seleccionada
                Videos seleccionado = (Videos) dcbm.getSelectedItem();
                lblPeli.setText(seleccionado.toString());
            }
        });
        //--------------------------------------------------------------------------------------------------------------

        //--------------------------------------------------------------------------------------------------------------
    }

    /**
     * Refrescar el comobox cada vez que damos de alta une pelicula
     */
    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for(Videos peli:lista){
            dcbm.addElement(peli);
        }
    }

    public static void main(String[] args) {
        Vista vista = new Vista();
    }

    /**
     * Damos de alta una pelicula
     * @param nombre
     * @param codigo
     */
    private void altaPeli(String nombre, String codigo){
        lista.add(new Videos(nombre,codigo));
    }

    /**
     * Creamos el menu con las opciones de importar y exportar
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");



        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            /**
             * Exportación del fichero
             */
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if(opcionSeleccionada==JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            /**
             * Importación del fichero
             */
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if(opcion==JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });
        menu.add(itemExportarXML);
        menu.add(itemImportarXML);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    /**
     * Importar XML
     * @param fichero
     */
    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro cada uno de los nodos coche para obtener sus campos
            NodeList pelis = documento.getElementsByTagName("peli");
            for (int i = 0; i < pelis.getLength(); i++) {
                Node peli = pelis.item(i);
                Element elemento = (Element) peli;

                //Obtengo los campos marca y modelo
                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String codigo = elemento.getElementsByTagName("codigo").item(0).getChildNodes().item(0).getNodeValue();

                altaPeli(nombre,codigo);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    /**
     * Exportar XML
     * @param fichero
     */
    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (coches) y lo añado al documento
            Element raiz = documento.createElement("pelis");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoVideo;
            Element nodoDatos;
            Text dato;

            //Por cada coche de la lista, creo un nodo coche
            for (Videos video : lista) {

                //Creo un nodo coche y lo añado al nodo raiz (coches)
                nodoVideo = documento.createElement("coche");
                raiz.appendChild(nodoVideo);

                //A cada nodo coche le añado los nodos marca y modelo
                nodoDatos = documento.createElement("nombre");
                nodoVideo.appendChild(nodoDatos);

                //A cada nodo de datos le añado el dato
                dato = documento.createTextNode(video.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("codigo");
                nodoVideo.appendChild(nodoDatos);

                dato = documento.createTextNode(video.getCodigo());
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un ficho de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }


}
